package com.adaptavist.confluence.apps.forms.model;

public enum ResponsesTablePermission {
    ADMIN_ONLY("ADMIN_ONLY");

    private final String permission;

    ResponsesTablePermission(final String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return this.permission;
    }
}
