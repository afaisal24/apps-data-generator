package com.adaptavist.confluence.apps.forms.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FormConfigRest {
    String name;
    String collectorType;
    List<DestinationView> destinations;
    String responsesTablePermission;
    boolean instantNotification;
    String description;
    String scriptRunnerEndpoint;
    String successHtml;
    String infoHtml;
    String submitter;
    String recipients;
}
