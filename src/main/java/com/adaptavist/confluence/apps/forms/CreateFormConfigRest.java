package com.adaptavist.confluence.apps.forms;

import com.adaptavist.confluence.apps.forms.client.FormsConfigClient;
import com.adaptavist.confluence.apps.forms.client.FormsConfigRestClient;
import com.adaptavist.confluence.apps.forms.model.CollectorType;
import com.adaptavist.confluence.apps.forms.model.DestinationView;
import com.adaptavist.confluence.apps.forms.model.FormConfigRest;
import com.adaptavist.confluence.apps.forms.model.ResponsesTablePermission;
import feign.Feign;
import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;

import java.util.ArrayList;

public class CreateFormConfigRest {
    private static final String CREATE_FORM_CONFIG_URL = "http://localhost:1990/confluence/rest/formservice/1.0/configs/formmail";
    private static final String FORM_NAME = "form-name";
    private static final String INFO_HTML = "Response: $response Destination endpoint: $endpoint FormID: $formID";
    private static final String USER_PASSW = "admin";

    public static void main(String[] args) {
        FormsConfigRestClient configClient = Feign.builder()
                .requestInterceptor(new BasicAuthRequestInterceptor(USER_PASSW, USER_PASSW))
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(FormsConfigClient.class))
                .logLevel(Logger.Level.FULL)
                .target(FormsConfigRestClient.class, CREATE_FORM_CONFIG_URL);

        FormConfigRest newConfig = new FormConfigRest(FORM_NAME, CollectorType.INTERNAL_INBOX.getCollectorType(), new ArrayList<DestinationView>(),
                ResponsesTablePermission.ADMIN_ONLY.getPermission(), true, "", "",
                "test", INFO_HTML, "", "");

        configClient.update(newConfig);
    }
}
