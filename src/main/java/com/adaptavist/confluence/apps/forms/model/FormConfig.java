package com.adaptavist.confluence.apps.forms.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FormConfig {
    String name;
    String collectorType;
    String destinations;
    String responsesTablePermission;
    boolean instantNotification;
    String description;
    String scriptRunnerEndpoint;
    String successHtml;
    String infoHtml;
    String submitter;
    String recipients;
}
