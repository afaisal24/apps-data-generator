package com.adaptavist.confluence.apps.forms.client;

import com.adaptavist.confluence.apps.forms.model.FormConfigRest;
import feign.Headers;
import feign.RequestLine;

public interface FormsConfigRestClient {
    @RequestLine("POST")
    @Headers({
            "Content-Type: application/json"
    })
    void create(FormConfigRest formConfigRest);

    @RequestLine("PUT")
    @Headers({
            "Content-Type: application/json"
    })
    void update(FormConfigRest formConfigRest);
}
