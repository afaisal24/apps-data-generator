package com.adaptavist.confluence.apps.forms.model;

public enum CollectorType {
    INTERNAL_INBOX("INTERNAL_INBOX");

    private final String type;

    CollectorType(final String type) {
        this.type = type;
    }

    public String getCollectorType() {
        return this.type;
    }
}
