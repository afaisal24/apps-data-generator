package com.adaptavist.confluence.apps.forms.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonAutoDetect
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DestinationView {

    private Long id;
    private String name;
    private String description;

    @JsonCreator
    public DestinationView(@JsonProperty("id") final Long id,
                           @JsonProperty("name") final String name,
                           @JsonProperty("description") final String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    //For Jackson purposes
    @SuppressWarnings("unused")
    public DestinationView() {
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonIgnore
    public String toString() {
        return "{" +
                "\"id\": \"" + id + '\"' +
                ", \"name\": \"" + name + '\"' +
                ", \"description\": \"" + description + '\"' +
                '}';
    }
}
