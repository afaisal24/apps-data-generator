package com.adaptavist.confluence.apps.forms;

import com.adaptavist.confluence.apps.forms.client.FormsConfigClient;
import com.adaptavist.confluence.apps.forms.model.CollectorType;
import com.adaptavist.confluence.apps.forms.model.FormConfig;
import com.adaptavist.confluence.apps.forms.model.ResponsesTablePermission;
import feign.Feign;
import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import feign.form.FormEncoder;
import feign.gson.GsonDecoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;

import java.util.concurrent.TimeUnit;

/**
 * The data generator for Forms config
 *
 * This generator submits new form config with the standard options:
 * - Form ID
 * - Internal database collector
 * - Instant notification
 * - Admin and space admin only permission
 * - Global space
 */
public class FormsConfigDataGenerator {
    private static final Integer FORMS_CONFIG_SIZE = 10; // The number of new Forms config you want to have
    private static final String INFO_HTML = "Response: $response Destination endpoint: $endpoint FormID: $formID";
    private static final String USER_PASSW = "admin";

    private static final String CREATE_FORM_CONFIG_URL = "http://localhost:8090/admin/forms/configuration-fmc-save-new.action?key=";

    public static void main(String[] args) {
        long startTime = System.nanoTime();
        int existingConfigCount = 0; // The existing config count that you have in the environment
        int newConfigCount = (FORMS_CONFIG_SIZE - existingConfigCount) + 1;

        FormsConfigClient configClient = Feign.builder()
                .requestInterceptor(new BasicAuthRequestInterceptor(USER_PASSW, USER_PASSW))
                .client(new OkHttpClient())
                .encoder(new FormEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(FormsConfigClient.class))
                .logLevel(Logger.Level.FULL)
                .target(FormsConfigClient.class, CREATE_FORM_CONFIG_URL);

        while(existingConfigCount <= FORMS_CONFIG_SIZE) {
            String name = "Form ".concat(String.valueOf(existingConfigCount));
            System.out.printf("Creating form %s\n", name);
            FormConfig newConfig = new FormConfig(name, CollectorType.INTERNAL_INBOX.getCollectorType(), "",
                    ResponsesTablePermission.ADMIN_ONLY.getPermission(), true, "", "",
                    "<h2>Successfully submitted!</h2>", INFO_HTML, "", "");

            configClient.create(newConfig);
            existingConfigCount++;
        }

        System.out.printf(
                "%d of new forms config created in %d minutes%n",
                newConfigCount,
                TimeUnit.MINUTES.convert(System.nanoTime() - startTime, TimeUnit.NANOSECONDS)
        );
    }
}
