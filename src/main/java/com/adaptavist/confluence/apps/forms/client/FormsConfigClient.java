package com.adaptavist.confluence.apps.forms.client;

import com.adaptavist.confluence.apps.forms.model.FormConfig;
import feign.Headers;
import feign.RequestLine;

public interface FormsConfigClient {
    @RequestLine("POST")
    @Headers({
            "Content-Type: application/x-www-form-urlencoded",
            "X-Atlassian-Token: nocheck"
    })
    void create(FormConfig formConfig);
}
